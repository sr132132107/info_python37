from info import create_app,db
#  导入flask——script包进行app的管理
from flask_script import Manager
#  导入flask_migrate包进行数据库的迁移
from flask_migrate import Migrate,MigrateCommand

#  利用工厂函数生产app.进行app的实例化
app = create_app('development')
# 实例化manager
manager = Manager(app)
#  实例化migrate
migrate = Migrate(app,db)
#  添加迁移命令
manager.add_command('db',MigrateCommand)





if __name__ == '__main__':
    print(app.url_map)
    # app.run(debug=True)
    #  使用manager
    manager.run()