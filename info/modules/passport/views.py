import re,random
from datetime import datetime
from flask import session

from info.models import User
from info.utils.response_code import RET
from . import passport_blue
from flask import request,jsonify,current_app,make_response
from info.utils.captcha.captcha import captcha
from info import redis_store,constants, db
from info.libs.yuntongxun.sms import CCP

#  定义图形验证码生产视图函数
@passport_blue.route('/image_code')
def generate_image_code():
    """生成图片验证码
    # var url = '/image_code?image_code_id=' + imageCodeId;
    1、获取uuid，image_code_id
    2、判断获取是否成功，如果没有uuid，直接终止程序运行
    3、调用captcha扩展包，生成图片验证码，name，text，image
    4、把图片验证码的内容，存入redis数据库，根据uuid作为后缀名
    5、把图片返回前端
    'http://127.0.0.1:5000/?a=1&b=' + 2
    http: // 127.0
    .0
    .1: 5000 /?a = 1 & b = 2
    :return:
    """
    #  获取参数
    image_code_id = request.args.get('image_code_id')
    #  判断获取是否成功，如果没有uuid，直接终止程序运行
    if not image_code_id:
        return jsonify(errno= RET.PARAMERR ,errmsg='参数缺失')
    #  判断成功，有UUID,调用captcha扩展包，生成图片验证码，name，text，image
    name,text,image = captcha.generate_captcha()
    #  把图片验证码的内容，存入redis数据库，根据uuid作为后缀名
    try:
        redis_store.setex('ImageCode_' + image_code_id, constants.IMAGE_CODE_REDIS_EXPIRES, text)
    except Exception as e:
        # 操作数据库，如果发生异常，需要记录项目日志
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='保存数据失败')
    # 把图片返回前端,使用响应对象，返回图片
    response = make_response(image)
    # 修改响应的数据类型，不修改浏览器可以解析，因为浏览器使用的img标签，但是如果通过测试工具测接口，无法显示图片！！
    response.headers['Content-Type'] = 'image/jpg'
    return response





#  发送短信后端实现
@passport_blue.route('/sms_code',methods=['POST'])
def sms_code():
    """ 发送短信
    获取参数----检查参数----业务处理----返回结果
    1、获取参数，前端ajax发送过来的手机号、图片验证码、图片验证码的编号(uuid)
    2、检查参数的完整性，mobile,image_code,image_code_id
    3、检查手机号格式，正则
    4、尝试从redis中获取真实的图片验证码，根据uuid来从redis中拿出唯一的图片验证码
    5、判断获取是否成功
    6、如果获取成功，应该把redis中存储的图片验证码给删了，因为图片验证码只能比较一次，所以只能读取redis数据库一次
    7、比较图片验证码是否正确
    8、生成6位数的随机数，作为短信验证码
    9、把短信验证码存入redis数据库中，便于后面用户注册时进行比较
    10、调用云通讯发送短信，保存发送结果
    11、判断发送是否成功
    12、返回结果"""
    #  接收mobile,image_code,image_code_id
    mobile = request.json.get('mobile')
    image_code = request.json.get('image_code')
    image_code_id = request.json.get('image_code_id')

    #  验证参数
    if not all([mobile,image_code,image_code_id]):
        return jsonify(errno=RET.PARAMERR,errmsg='参数错误')
    #  验证手机号格式
    if not re.match(r'1[3456789]\d{9}$',mobile):
        return jsonify(errno=RET.PARAMERR,errmsg='手机号格式错误')
    #  尝试从redis中获取真实的图片验证码，根据uuid来从redis中拿出唯一的图片验证码
    try:
        real_image_code = redis_store.get('ImageCode_' + image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno= RET.DATAERR,errmsg='获取数据失败')
    #  判断验证码从redis中获取的是否过期
    if not real_image_code:
        return jsonify(errno=RET.NODATA, errmsg='数据已过期')
    """如果获取成功，应该把redis中存储的图片验证码给删了，
        因为图片验证码只能比较一次，所以只能读取redis数据库一次"""
    try:
        redis_store.delete('ImageCode_' + image_code_id)
    except Exception as e:
        current_app.logger.error(e)
    #  比较图片验证码是否正确，忽略大小写
    if real_image_code.lower() != image_code.lower():
        return jsonify(errno=RET.DATAERR, errmsg='图片验证码错误')
    #  验证手机号是否已经被注册过
    try:
        user = User.query.filter(User.mobile==mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg='查询用户数据失败')
    else:
        if user is not None:
            return jsonify(errno=RET.DATAEXIST,errmsg='手机号已注册')

    #  生成6位数的随机数，作为短信验证码
    sms_code = '%06d'% random.randint(0,999999)
    #  把短信验证码存入redis数据库中，便于后面用户注册时进行比较
    print(sms_code)
    try:
        redis_store.setex('sms_code'+mobile,constants.SMS_CODE_REDIS_EXPIRES,sms_code)
    except Exception as e:
        current_app.logger.error(e)
    #  调用云通讯发送短信，保存发送结果,调用第三方接口需要异常捕获处理
    try:
        ccp = CCP()
        result = ccp.send_template_sms(mobile, [sms_code,constants.SMS_CODE_REDIS_EXPIRES/60],1)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR,errmsg='发送短信异常')

    #  判断发送是否成功
    if result == 0:
        return jsonify(errno=RET.OK,errmsg='发送成功')
    else:
        return jsonify(errno=RET.THIRDERR,errmsg='发送失败')




#  实现注册
@passport_blue.route('/register',methods=['POST'])
def register():
    #  接收参数   分析参数/判断参数   业务处理  返回结果
    #接收参数
    mobile = request.json.get('mobile')
    smscode = request.json.get('smscode')
    password = request.json.get('password')
    #  判断三个参数的完整性
    if not all([mobile,smscode,password]):
        return jsonify(errno=RET.PARAMERR,errmsg='参数错误')
    #  验证手机格式
    if not re.match(r'1[3456789]\d{9}$',mobile):
        return jsonify(errno=RET.PARAMERR,errmsg='手机号格式错误')
    #  判断该手机号是否已经注册过，查询mysql数据库
    try:
        user =User.query.filter(User.mobile==mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg='查询用户数据失败')
    else:
        if user is not None:
            return jsonify(errno=RET.DATAEXIST,errmsg='手机号已注册')
    #  短信验证码验证，从redis数据库中拿取
    try:
        real_smscode = redis_store.get('sms_code'+mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg='查询数据失败')
    #  验证从redis中拿出的数据是否过期
    if not real_smscode:
        return jsonify(errno=RET.NODATA,errmsg='数据已过期')
    #  与从前端接收到的数据进行比较，验证短信验证码的对错
    if real_smscode != str(smscode):
        return jsonify(errno=RET.DATAERR,errmsg='短信验证码错误')
    #  短信验证正确后从redis数据库中删除短信验证码,数据在redis是以键值队形式存储
    try:
        redis_store.delete('SMSCode_' + mobile)
    except Exception as e:
        current_app.logger.error(e)

    #  构造模型类对象保存用户数据，对接收到的密码进行加密。实例化user
    user = User()
    user.mobile = mobile
    user.nick_name = mobile
    user.password = password
    #  提交用户信息到mysql数据库中
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        #  数据提交不成功，需要回滚数据
        db.session.rollback()
        return jsonify(errno=RET.DBERR,errmsg='保存数据失败')
    #  缓存用户信息，实现状态保持
    session['user_id'] = user.id
    session['mobile'] = mobile
    session['nick_name'] = mobile
    #   返回注册状态给前端
    return jsonify(errno=RET.OK,errmsg='注册成功')


#  实现登录
@passport_blue.route('/login',methods=['POST'])
def login():
    #  接收参数   分析/验证参数   业务处理    返回结果
    mobile = request.json.get('mobile')
    password = request.json.get('password')
    #  验证参数的完整性
    if not all([mobile,password]):
        return jsonify(errno=RET.PARAMERR,errmsg='参数错误')
    # 检查手机号格式
    if not re.match(r'1[3456789]\d{9}$', mobile):
        return jsonify(errno=RET.PARAMERR, errmsg='手机号格式错误')
    #   验证手机号和密码与注册的时候是否相同，需要在mysql中查询该用户是否注册过
    try:
        user = User.query.filter(User.mobile == mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg='查询数据失败')
    else:
        #   密码验证需要对从前端获取到的密码重新hash加密后才能比较，调用了models里面的check-password方法
        if not user or (not user.check_password(password)):
            return jsonify(errno=RET.PWDERR,errmsg='用户名或密码错误')
    #  帐号和密码验证通过后需要进行状态保持，记录登录的时间
    user.last_login = datetime.now()
    #   将登录时间写入到mysql数据库中
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        #  数据保存不成功需要回滚
        db.session.rollback()
        return jsonify(errno=RET.DBERR,errmsg='保存数据失败')
    #   实现状态保持
    session['user_id']= user.id
    session['mobile'] = user.mobile
    session['nick_name'] =user.nick_name
     #  返回结果给前端
    return jsonify(errno=RET.OK,errmsg='登录成功')


#   实现退出功能
@passport_blue.route('/logout')
def logout():

    #  退出的本质是清除用户redis中缓存的用户信息
    session.pop('user_id',None)
    session.pop('mobile',None)
    session.pop('nick_name',None)

    return jsonify(errno=RET.OK,errmsg='退出成功')










    pass






