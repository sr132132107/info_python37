from redis import StrictRedis
from flask import Flask
from flask_session import Session
#  导入flask_SQLAlchemy包与python交互
from flask_sqlalchemy import SQLAlchemy
#  导入配置对象
from config import config_dict

from config import Config
#  从flask-wtf中导入CSRFProtect，csrf
from flask_wtf import CSRFProtect,csrf
#  实例化数据库sqlalchemy
db = SQLAlchemy()


#   实例化redis数据库，用来存储与业务相关的数据，比如图形验证码、手机验证码,默认从redis中拿出来的响应数据为bites类型
redis_store = StrictRedis(host=Config.REDIS_HOST,port=Config.REDIS_PORT,decode_responses=True)

#  导入日志模块
import logging
from logging.handlers import RotatingFileHandler

# 设置日志的记录等级
logging.basicConfig(level=logging.DEBUG) # 调试debug级
# 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024*1024*100, backupCount=10)
# 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
# 为刚创建的日志记录器设置日志记录格式
file_log_handler.setFormatter(formatter)
# 为全局的日志工具对象（flask app使用的）添加日志记录器
logging.getLogger().addHandler(file_log_handler)

#  定义工厂函数，生产app，可以改变代码执行顺序
#  动态的修改程序的配置



def create_app(config_name):
    #  实例化app
    app = Flask(__name__)
    app.config.from_object(config_dict[config_name])
    
    #  实例化csrfprotect对象
    CSRFProtect(app)
    #  利用请求钩子，在每一次请求结束后通过响应给response给前端浏览器写入带有cookie的带有csrf的保护令牌
    @app.after_request
    def after_request(response):
        csrf_token = csrf.generate_csrf()
        response.set_cookie('csrf_token',csrf_token)
        return response

    
    #  让session扩展和程序实例关联起来
    Session(app)
    db.init_app(app)

    #  注册蓝图
    from info.modules.news import news_blue
    app.register_blueprint(news_blue)
    from info.modules.passport import passport_blue
    app.register_blueprint(passport_blue)
    from info.modules.profile import profile_blue
    app.register_blueprint(profile_blue)


    #  导入自定义的过滤器
    from info.utils.commons import index_filter
    #  将自定义的过滤器加入到程序实例内置的过滤器中
    app.add_template_filter(index_filter,'index_filter')
    
    return app