from . import profile_blue
from info.utils.commons import login_required

from flask import g,render_template,redirect,request
@profile_blue.route('/info')
@login_required
def user_info():
    """
    用户基本信息显示
    1、尝试获取用户信息
    2、如果用户未登录，重定向到项目首页
    3、如果用户已登录，调用模型类中的to_dict函数，获取用户信息
    4、传给模板

    :return:
    """
    user = g.user
    if not user:
        return redirect('/')


    data = {
        'user':user.to_dict()

    }






    return render_template('news/user.html',data= data)

#  创建iframe子网页
#  基本资料
@profile_blue.route('/base_info',methods=['GET','POST'])
@login_required
def base_info():
    user = g.user
    if request.method == 'GET':
        data = {
            'user': user.to_dict()
        }
        return render_template('news/user_base_info.html', data=data)









#  头像设置
@profile_blue.route('/pic_info',methods=['GET','POST'])
@login_required
def pic_info():




   return render_template('news/user_pic_info.html')








#  我的关注
@profile_blue.route('/user_follow',methods=['GET','POST'])
@login_required
def user_follow():



   return render_template('news/user_follow.html')





#  密码修改
@profile_blue.route('/pass_info',methods=['GET','POST'])
@login_required
def pass_info():



   return render_template('news/user_pass_info.html')




#  我的收藏
@profile_blue.route('/collection',methods=['GET','POST'])
@login_required
def collection():



   return render_template('news/user_collection.html')


#   新闻发布
@profile_blue.route('/news_release',methods=['GET','POST'])
@login_required
def news_release():



   return render_template('news/user_news_release.html')





#  新闻列表
@profile_blue.route('/news_list',methods=['GET','POST'])
@login_required
def news_list():



   return render_template('news/user_news_list.html')