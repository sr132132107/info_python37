from flask import session,current_app,g
from info.models import User


#  自定义过滤器
def index_filter(index):
    if index == 0:
        return 'first'
    elif index == 1:
        return 'second'
    elif index == 2:
        return 'third'
    else:
        return ''

#  定义一个装饰器，用来验证用户是否登录
def login_required(f):
    def wrapper(*args,**kwargs):

    #  验证用户的登录状态
        #  尝试从redis中获取用户的session信息
        user_id = session.get('user_id')

        #  定义user 为全局变量
        user = None
        #  redis中session有用户信息，根据user_id查询用户信息
        if user_id:
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
        # 使用应用上下文对象g，来临时存储用户数据，
        # 把查询结果的user对象，赋值给g的属性存储
        g.user = user
        return f(*args,**kwargs)
    #  封装代码后，在视图函数中使用装饰器，发现被装饰的函数名变成了wrapper！
    # 装饰器修改了被装饰的函数的属性__name__
    wrapper.__name__ = f.__name__
    return wrapper