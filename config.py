#  导入redis数据库进行session状态保持
from redis import StrictRedis



class Config():
    DEBUG = True
    #  配置密钥
    SECRET_KEY = "jW63qcb9HM7mwEU1VjyjaCRznfjWSeSqs7SHrFqFtE5mIC4wXt9O=="
    #  配置状态保持session的存储
    SESSION_TYPE = 'redis'
    #  抽取redis主机和端口
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379

    #  配置redis数据库的连接
    SESSION_REDIS = StrictRedis(host=REDIS_HOST,port=REDIS_PORT)
    #  配置session签名
    SESSION_USE_SIGNER = True
    #  配置session状态保持时长
    PERMANENT_SESSION_LIFETIME = 86400
    #  配置数据库的连接
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@localhost/info_python37'
    #  配置数据库的动态跟踪修改
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEBUG = True

class ProductionConfig(Config):
    DEBUG = False


config_dict = {
                'development' : DevelopmentConfig,
                'production': ProductionConfig
}
